#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>

TAR=kunststoff_$2.orig.tar.gz

# clean up the upstream tarball
unzip $3
tar -c -z -f $TAR src/* licence.txt
rm -rf $3 src/ licence.txt kunststoff.jar

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

exit 0
